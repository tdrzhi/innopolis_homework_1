package tdorzhi.homework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import tdorzhi.homework.resources.Resource;
import tdorzhi.homework.resources.Readable;
import tdorzhi.homework.workers.EvenNumberWorkerManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class Main {

    private static Logger logger = LoggerFactory.getLogger(Main.class);
    private static final ApplicationContext appContext = new ClassPathXmlApplicationContext(new String[]{"appContext.xml"});

    public static void main(String[] args) throws ClassNotFoundException {
        if (args.length > 0) {
            String[] strings = args;
            if ("--interactive".equals(args[0]) || "-i".equals(args[0])) {
                logger.info("Enter resources separating them by new line (Enter), on the end, type 'end': ");
                try {
                    strings = readFromConsole();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                    logger.error("Failed to fetch resource");
                    return;
                }
            }
            EvenNumberWorkerManager manager;
            try {
                Readable[] resources = new Resource[strings.length];
                for (int i = 0; i < strings.length; i++) {
                    resources[i] = new Resource(strings[i]);
                }
                manager = (EvenNumberWorkerManager) appContext.getBean("manager", (Object) resources);
                manager.runWorkers();
            } catch (InterruptedException e) {
                logger.error("One or more workers were interrupted");
                logger.error(e.getCause().getMessage());
            } catch (ExecutionException e) {
                logger.error("Exception thrown by one or more workers!!");
                logger.error(e.getCause().getMessage());
            } catch (IOException e) {
                logger.error("Error while attempting to fetch resource");
                logger.error(e.getCause().getMessage());
            }
        } else {
            logger.warn("No resources defined");
        }
        logger.debug("Exiting program");
    }

    private static String[] readFromConsole() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<String> strings = new ArrayList<>();
        String[] result;
        while (true) {
            String inputString = reader.readLine();
            if (!"end".equalsIgnoreCase(inputString)) {
                strings.add(inputString);
            } else {
                break;
            }
        }
        if (strings.isEmpty()) {
            result = new String[0];
        } else {
            result = strings.toArray(new String[strings.size()]);
        }
        return result;
    }
}
