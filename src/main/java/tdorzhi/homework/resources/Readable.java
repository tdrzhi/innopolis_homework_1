package tdorzhi.homework.resources;

import java.io.BufferedReader;

/**
 * Created by dixi on 17.12.16.
 */
public interface Readable extends AutoCloseable {
    BufferedReader getReader();
    String getResourceName();

    @Override
    void close();
}
