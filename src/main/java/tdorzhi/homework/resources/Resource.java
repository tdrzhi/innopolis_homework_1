package tdorzhi.homework.resources;

import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;


/**
 * Created by dixi on 10.12.16.
 * Класс занимается выдачей буферизованного входного потока пользователю класса
 */
public class Resource implements Readable {

    private static Logger logger = LoggerFactory.getLogger(Resource.class);

    @Override
    public String getResourceName() {
        return resourceName;
    }

    /**
     * Ссылка или путь к ресурсу
     */
    private String resourceName;

    @Override
    public BufferedReader getReader() {
        return reader;
    }

    /**
     * Поток на чтение
     */
    private BufferedReader reader;

    /**
     * Создает объект класса Resource
     * @param resource строка, представляющая из себя либо путь к файлу, либо URL
     * @throws IOException исключение выбрасывается при неудавшейся попытке открыть ресурс
     */
    public Resource(String resource) throws IOException {
        resourceName = resource;
        if (resourceName != null) {
            if (resourceName.matches("(http://|ftp://|https://)?(www\\.)?[\\w]+\\.[\\w]+/?[\\w\\d.]*")) {
                logger.debug("opening url");
                reader = openUrl(resourceName);
            } else {
                logger.debug("opening file");
                reader = openFile(resourceName);
            }
        }
    }

    /**
     * Открывает соединение с resourceURL
     * @param resourceURL ссылка на удаленный ресурс
     * @throws IOException в случае ошибки установления соединения или неправильной ссылки
     * @return null в случае неудачного установления соединения или неправильной ссылки, иначе BufferedReader
     */
    private BufferedReader openUrl(String resourceURL) throws IOException {
        BufferedReader reader = null;
        if (!(resourceURL.startsWith("http://")
                        || resourceURL.startsWith("ftp://")         // we checked that this is url without protocol prepended
                        || resourceURL.startsWith("https://"))) {   // so we prepend one
            resourceURL = "http://" + resourceURL;
        }
        try {
            URLConnection connect = new URL(resourceURL).openConnection();
            connect.setConnectTimeout(10_000);
            reader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
        } catch (MalformedURLException e) {
            logger.error("Wrong URL format");
        }
        return reader;
    }

    /**
     * Открывает файл resourcePath для чтения
     * Не распознает относительные пути без префикса с точкой, как например ./
     * @param resourcePath путь до файла
     * @throws FileNotFoundException в случае если файл не был найден, или к нему нет доступа
     * @return null в случае ошибки доступа к файлу, иначе BufferedReader
     */
    private BufferedReader openFile(String resourcePath) throws FileNotFoundException{
        BufferedReader reader;
        File file = new File(resourcePath);
        reader = new BufferedReader(new FileReader(file));
        return reader;
    }

    /**
     * Метод интерфейса Autocloseable
     * Закрывает поток reader (по крайней мере попытается)
     */
    @Override
    public void close() {
        try {
            reader.close();
        } catch (IOException e) {
            reader = null;
        } finally {
            reader = null;
        }
    }
}
