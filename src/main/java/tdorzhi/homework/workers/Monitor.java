package tdorzhi.homework.workers;

import java.math.BigInteger;

/**
 * Created by dixi on 18.12.16.
 * Не нашел другого способа синхронизировать на BigInteger
 */
public class Monitor {
    private volatile BigInteger value = BigInteger.ZERO;
    private volatile boolean error;

    public BigInteger getValue() {
        return value;
    }

    public void add(BigInteger value) {
        this.value = this.value.add(value);
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}