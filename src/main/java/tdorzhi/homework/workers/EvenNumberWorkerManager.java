package tdorzhi.homework.workers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import tdorzhi.homework.resources.Readable;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by innopolis on 15.12.2016.
 * Заготовка на случай клиент-серверной архитектуры
 */
public class EvenNumberWorkerManager {

    private static Logger logger = LoggerFactory.getLogger(EvenNumberWorkerManager.class);
    private static ApplicationContext context = new ClassPathXmlApplicationContext("appContext.xml");

    private List<EvenNumberWorker> workers;
    private volatile Monitor total;
    private volatile Lock locker;

    public Monitor getTotal() {
        return total;
    }

    public EvenNumberWorkerManager(Readable[] resources) throws IOException {
        workers = new LinkedList<>();
        total = new Monitor();
        locker = new ReentrantLock();

        for (Readable resourceString : resources) {
            workers.add((EvenNumberWorker) context.getBean("worker", resourceString, total, locker));
        }
    }

    public void runWorkers() throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(workers.size());
        if (workers.size() > 0) {
            List<Future<Boolean>> futures = executor.invokeAll(workers);
            executor.shutdownNow();
            if (!total.isError()) {
                logger.info("Total sum: " + total.getValue());
            }
        } else {
            logger.error("No resources defined");
        }
    }


}
