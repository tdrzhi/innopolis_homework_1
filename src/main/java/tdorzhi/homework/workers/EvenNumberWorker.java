package tdorzhi.homework.workers;

import org.apache.log4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdorzhi.homework.processors.Parser;
import tdorzhi.homework.processors.Processor;
import tdorzhi.homework.processors.Validator;
import tdorzhi.homework.resources.Readable;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.Lock;

import static tdorzhi.homework.processors.Validator.BigIntegerValidator.isEven;
/**
 * Created by dixi on 17.12.16.
 *
 */
class EvenNumberWorker implements Callable<Boolean> {
    private Logger logger = LoggerFactory.getLogger(EvenNumberWorker.class);

    private Readable readable;
    private BigInteger localSum;
    private Monitor globalSum;
    private Lock locker;

    EvenNumberWorker(Readable resource, Monitor globalSum, Lock lock) {
        this.localSum = BigInteger.ZERO;
        this.readable = resource;
        this.globalSum = globalSum;
        this.locker = lock;
    }

    @Override
    public Boolean call() {
        MDC.put("Resource", readable.getResourceName());

        Processor<BigInteger> validator = new Validator<>(BigInteger.class);
        Processor<String> parser = new Parser<>(BigInteger.class);

        Iterator<String> linesIterator = readable.getReader().lines().iterator();
        while (!globalSum.isError() && linesIterator.hasNext()) {
            if (parser.process(linesIterator.next())) {
                //TODO: check this if got unchecked cast exception
                for (BigInteger integer: (List<BigInteger>) parser.getProcessed()) {
                    if (validator.process(integer)) {
                        if (integer.compareTo(BigInteger.ZERO) >= 0 && isEven(integer)) {
                            localSum = localSum.add(integer);

                            locker.lock();
                            globalSum.add(integer);
                            logger.info(globalSum.getValue().toString());
                            locker.unlock();

                        }
                    } else {
                        logger.error("Resource contains number out of integer bounds");
                        globalSum.setError(true);
                    }
                }
            } else {
                logger.error("Resource contains invalid data");
                globalSum.setError(true);
            }
        }
        readable.close();
        MDC.clear();
        if (!globalSum.isError()) {
            logger.info("Resource "+ readable.getResourceName()+ " sum: " + localSum);
        }
        return Boolean.TRUE;
    }
}
