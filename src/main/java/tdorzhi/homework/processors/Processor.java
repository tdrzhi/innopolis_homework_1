package tdorzhi.homework.processors;

import java.util.List;

/**
 * Created by dixi on 18.12.16.
 * Основная идея класса в том, чтобы наследники могли расширять функционал в зависимости от переданного
 * класса в конструкторе
 * T здесь некоторый тип, который наследник должен обработать при вызове process()
 * @see Validator
 * @see Parser
 * @see Class
 */
public abstract class Processor<T> {

    private Class clazz;

    protected Class getClazz() {
        return clazz;
    }

    public Processor(Class clazz) {
        this.clazz = clazz;
    }

    public abstract List<?> getProcessed();

    public abstract boolean process(T toProcess);
}
