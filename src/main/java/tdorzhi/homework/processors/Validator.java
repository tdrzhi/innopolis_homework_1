package tdorzhi.homework.processors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by dixi on 17.12.16.
 * Параметризованный класс-валидатор с возможностью расширения функционала
 * Как и все потомки Processor, валидатор имеет возможность расширения функционала в зависимости от переданного
 * в конструкторе класса
 * @see Processor
 * @see BigIntegerValidator
 */
public class Validator<T> extends Processor<T> {

    private static Logger logger = LoggerFactory.getLogger(Validator.class);

    public Validator(Class validatedClass) {
        super(validatedClass);
    }

    @Override
    public boolean process(T toProcess) {
        return validate(toProcess);
    }

    @Override
    public List<?> getProcessed() {
        return null;
    }

    public boolean validate(T toValidate) {
        if (toValidate.getClass() == this.getClazz()) {
            if (this.getClazz() == BigInteger.class) {
                BigIntegerValidator integerValidator = new BigIntegerValidator(Integer.MIN_VALUE, Integer.MAX_VALUE);
                return integerValidator.isValid((BigInteger) toValidate);
            }
        }
        return false;
    }

    /**
     * Вложенный класс BigIntegerValidator занимается валидацией только BigInteger
     * Доступны статические методы
     */
    public static class BigIntegerValidator {

        private long biggerOrEqualThan;
        private long lessOrEqualThan;

        public BigIntegerValidator(long biggerOrEqualThan, long lessOrEqualThan) {
            this.biggerOrEqualThan = biggerOrEqualThan;
            this.lessOrEqualThan = lessOrEqualThan;
        }

        boolean isValid(BigInteger element) {
            return inBounds(element, lessOrEqualThan, biggerOrEqualThan);
        }

        public static boolean isEven(BigInteger bigInt) {
            return (bigInt.and(BigInteger.ONE)).equals(BigInteger.ZERO);
        }

        public static boolean inBounds(BigInteger integer, long topCapacity, long bottomCapacity) {
            return integer.compareTo(BigInteger.valueOf(bottomCapacity)) >= 0 && integer.compareTo(BigInteger.valueOf(topCapacity)) <= 0;
        }
    }
}
