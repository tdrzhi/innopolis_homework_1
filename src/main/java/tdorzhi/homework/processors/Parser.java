package tdorzhi.homework.processors;

import org.apache.log4j.MDC;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.*;

/**
 * Created by dixi on 17.12.16.
 * Класс реализует абстрактный класс Processor, основная задача в зависимости от переданного в конструкторе класса,
 * распарсить поступивший в методе process объект некоторого класса T и результат парсинга вернуть в методе getProcessed
 * На данный момент класс только занимается парсингом String типа данных на BigInteger объекты
 * @see Processor
 */
public class Parser<T> extends Processor<T> {
    private static Logger logger = LoggerFactory.getLogger(Parser.class);

    private char[] delims;
    private boolean stopOnError;
    private List<?> processed;

    public static final char[] WHITESPACE_DELIMS = {' ', '\t', '\n'};

    @Override
    public boolean process(T toProcess) {
        return parse((String) toProcess);
    }

    @Override
    public List<?> getProcessed() {
        return processed;
    }

    public Parser(Class toProcess) {
        this(toProcess, WHITESPACE_DELIMS, true);
    }

    public Parser(Class toProcess, char[] delimiters) {
        this(toProcess, delimiters, true);
    }

    public Parser(Class toProcess, char[] delimiters, boolean stopOnError) {
        super(toProcess);
        this.delims = delimiters;
        this.stopOnError = stopOnError;
    }

    public boolean parse(String toParse) {
        boolean isSuccessful = true;
        if (this.getClazz() == BigInteger.class) {
            BigIntegerParser bigIntParser = new BigIntegerParser();
            if (!bigIntParser.parse(toParse)) {
                isSuccessful = false;
            }
            this.processed = bigIntParser.getParsedList();
        }
        return isSuccessful;
    }

    class BigIntegerParser {

        private List<BigInteger> parsedList;

        public List<BigInteger> getParsedList() {
            return parsedList;
        }

        /**
         * Парсит строку на BigInteger и заполняет ими parsedList
         * @see this.parsedList
         * @return false, если найден хотя бы один элемент строки не являющийся/не приводимый к BigInteger
         */
        public boolean parse(String string) {
            boolean isSuccessful = true;
            parsedList = new ArrayList<>();
            StringTokenizer tokenizer = new StringTokenizer(string, Arrays.toString(Parser.this.delims));
            while (tokenizer.hasMoreTokens()) {
                String token = tokenizer.nextToken();
                MDC.put("Token", token);
                try {
                    parsedList.add(new BigInteger(token));
                } catch (NumberFormatException e) {
                    logger.error("Could not parse big integer");
                    isSuccessful = false;
                } finally {
                    MDC.clear();
                }
                if (!isSuccessful && Parser.this.stopOnError) {
                    break;
                }
            }
            return isSuccessful;
        }
    }
}
