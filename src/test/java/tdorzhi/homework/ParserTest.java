package tdorzhi.homework;

/**
 * Created by innopolis on 15.12.2016.
 */

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdorzhi.homework.processors.Parser;

import java.math.BigInteger;

public class ParserTest {

    private static Logger logger = LoggerFactory.getLogger(ParserTest.class);
    private Parser parser;

    @Test
    public void stopsOnError() {
        parser = new Parser(BigInteger.class, Parser.WHITESPACE_DELIMS, true);
        Assert.assertFalse(parser.parse("12312382183 5564564 0546545 this 564654string doesn't containt bigintegers"));
        Assert.assertTrue(parser.parse("65464654 2123 3  2 6 465 165 45 65 16  15654 "));
    }


}
