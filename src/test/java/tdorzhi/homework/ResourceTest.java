package tdorzhi.homework;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdorzhi.homework.resources.Resource;

import java.io.IOException;

/**
 * Created by innopolis on 15.12.2016.
 */
public class ResourceTest {
    private static Logger logger = LoggerFactory.getLogger(ResourceTest.class);
    private Resource resource;

    @Test(expected = IOException.class)
    public void invalidURL() throws IOException {
        resource = new Resource("http://dixi.q/valid0.txt");
        resource.close();
    }

    @Test
    public void validURIs() throws IOException {
        resource = new Resource("dixi.tk/valid1.txt");
        resource.close();
        Assert.assertNull(resource.getReader());
        Assert.assertNotNull(new Resource("dixi.tk/valid0.txt").getReader());
        Assert.assertNotNull(new Resource("/home/dixi/ownCloud/Study/innopolis/IdeaProjects/innopolis_homework_1/version.properties").getReader());
    }
}
