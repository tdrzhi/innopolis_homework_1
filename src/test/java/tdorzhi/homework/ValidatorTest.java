package tdorzhi.homework;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdorzhi.homework.processors.Validator;

import java.math.BigInteger;

/**
 * Created by dixi on 18.12.16.
 */

public class ValidatorTest {
    private static Logger logger = LoggerFactory.getLogger(ValidatorTest.class);

    @Test
    public void classMismatch() {
        Validator<String> validator = new Validator<>(BigInteger.class);
        Assert.assertFalse(validator.validate("invalid data"));
    }

    @Test
    public void validatorTest() {
        Validator<BigInteger> validator = new Validator<>(BigInteger.class);
        Assert.assertTrue(validator.validate(new BigInteger(String.valueOf(Integer.MAX_VALUE))));
        Assert.assertFalse(validator.validate(new BigInteger("2384238482384823848234823848238428384823")));
    }

    @Test
    public void getNoData() {
        Validator<BigInteger> validator = new Validator<>(BigInteger.class);
        Assert.assertNull(validator.getProcessed());
    }
}
