package tdorzhi.homework;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdorzhi.homework.resources.Readable;
import tdorzhi.homework.resources.Resource;
import tdorzhi.homework.workers.EvenNumberWorkerManager;

import java.io.IOException;
import java.math.BigInteger;
import java.util.concurrent.ExecutionException;

/**
 * Created by dixi on 18.12.16.
 */
public class ManagerTest {
    private static Logger logger = LoggerFactory.getLogger(ManagerTest.class);
    private EvenNumberWorkerManager manager;

    @Test
    public void basicCheck() throws IOException, ExecutionException, InterruptedException {
        Readable[] resources = new Resource[1];
        resources[0] = new Resource("http://dixi.tk/valid0.txt");
        manager = new EvenNumberWorkerManager(resources);
        manager.runWorkers();
        Assert.assertFalse(manager.getTotal().isError());
        Assert.assertTrue(manager.getTotal().getValue().equals(BigInteger.valueOf(291443896592L)));
    }

}
